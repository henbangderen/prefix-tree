package levakin.prefix.string.tree;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class TreeProber
{
    Logger log = LoggerFactory.getLogger(getClass());
    public boolean equals(char x, char fromTree){
        return x == fromTree;
    }

    public boolean contains(char[] stringToFind, List<Tree> children)
    {
        for (Tree t : children){
            if (contains(stringToFind, t)){
                return true;
            }
        }
        return false;
    }

    public boolean contains(char[] stringToFind, Tree tree){
        char left = stringToFind[0];
        char right = tree.getLetter();
        log.info("left: " + left +  " right: " + right );

        if(stringToFind.length == 1 &&
           left == right){
            return true;
        }

        if (left == right){
            String tmp = new String(stringToFind);
            String tmp2 = tmp.substring(1);
            char[] tail = tmp2.toCharArray();
            List<Tree> children = tree.getChildren();
            return contains(tail, children);
        }
        return false;
    }
}
