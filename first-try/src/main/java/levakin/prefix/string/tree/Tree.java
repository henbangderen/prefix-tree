package levakin.prefix.string.tree;

import java.util.List;

public class Tree
{
    char letter;
    List<Tree> children;

    public Tree(char letter, List<Tree> children)
    {
        this.letter = letter;
        this.children = children;
    }

    public Tree(char letter)
    {
        this.letter = letter;
    }

    public boolean isLeaf(Tree tree){
        return children.isEmpty();
    }

    public char getLetter(){
        return letter;
    }

    public List<Tree> getChildren()
    {
        return children;
    }

    public void setChildren(List<Tree> children)
    {
        this.children = children;
    }

    public void setLetter(char letter)
    {
        this.letter = letter;
    }

    @Override
    public String toString()
    {
        return "Tree{" +
                "letter=" + letter +
                ", children=" + children +
                '}';
    }
}
