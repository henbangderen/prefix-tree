package levakin.prefix.string.tree;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TreeTest
{
    Logger log = LoggerFactory.getLogger(getClass());
    @Test
    public void tt(){

        List<String> list = Arrays.asList("foo", "bar");
    }

    @Test
    public void oneNode(){
        Tree tree = new Tree('t', Collections.EMPTY_LIST);
        log.info("tree: " + tree);

        TreeProber treeProber = new TreeProber();
        boolean actual = treeProber.contains(new char[] {'t'}, tree );
        log.info("output: " + actual);
        assertTrue(actual);
    }
    @Test
    public void oneNodeNegative(){
        Tree tree = new Tree('t', Collections.EMPTY_LIST);
        log.info("tree: " + tree);

        TreeProber treeProber = new TreeProber();
        boolean actual = treeProber.contains(new char[] {'A'}, tree );
        log.info("output: " + actual);
        assertFalse(actual);
    }

    @Test
    public void twoLetters(){
        Tree tree = new Tree('t',
                Arrays.asList(new Tree('e', Collections.EMPTY_LIST)));
        log.info("tree: " + tree);

        TreeProber treeProber = new TreeProber();
        boolean actual = treeProber.contains(new char[] {'t', 'e'}, tree );
        log.info("output: " + actual);
        assertTrue(actual);
    }
    @Test
    public void twoLettersNegative(){
        Tree tree = new Tree('t',
                Arrays.asList(new Tree('e', Collections.EMPTY_LIST)));
        log.info("tree: " + tree);

        TreeProber treeProber = new TreeProber();
        boolean actual = treeProber.contains(new char[] {'t', 'e', 's'}, tree );
        log.info("output: " + actual);
        assertFalse(actual);
    }

    @Test
    public void smoke(){
        Tree tree = new Tree('t',
                Arrays.asList(new Tree('e',
                                    Arrays.asList(new Tree('s',
                                            Arrays.asList(new Tree('t', Collections.EMPTY_LIST)))))));
        log.info("tree: " + tree);

        TreeProber treeProber = new TreeProber();
        boolean actual = treeProber.contains(new char[] {'t', 'e', 's', 't'}, tree );
        log.info("output: " + actual);
        assertTrue(actual);
    }
}