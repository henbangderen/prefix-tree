# prefix-tree-parent

Experiments with a Trie
https://en.wikipedia.org/wiki/Trie

## Build
```	
mvn clean package
```

## Test
```	
cd prefix-tree
mvn test
```

## Example
[here](./prefix-tree/src/test/java/levakin/prefix/string/tree/Example.java)

### License
Copyright © 2021 Roman Levakin. Distributed under the Eclipse Public License 1.0
