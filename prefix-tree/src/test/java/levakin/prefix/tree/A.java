package levakin.prefix.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class A implements Comparable {
    private final String val;
    public A(String val) {
        this.val = val;
    }

    public static A neo(String id){
        return new A(id);
    }

    public static List<A> neos(String[] ids){
        List<A> result = new ArrayList<>();
        for (String id : ids){
            result.add(new A(id));
        }
        return result;
    }

    public static List<List<A>> neos(List<String> words){
        List<List<A>> result = new ArrayList<>();
        for (String w : words){
            List<A> aWord = new ArrayList<>();
            for (Character c : w.toCharArray()){
                aWord.add(new A(c.toString()));
            }
            result.add(aWord);
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof A)) return false;
        A a = (A) o;
        return Objects.equals(val, a.val);
    }

    @Override
    public int hashCode() {
        return Objects.hash(val);
    }

    @Override
    public String toString() {
        return "'id:" + val + '\'';
    }

    @Override
    public int compareTo(Object o) {
        return this.toString().compareTo(o.toString());
    }
}
