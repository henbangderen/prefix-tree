package levakin.prefix.tree;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class TrieCommanderAddTest extends Tester<A> {

    @Test
    public void smoke(){
        System.out.println(new File(".").getAbsolutePath());
        addPositive(Arrays.asList(
                Arrays.asList(new A("t"), new A("o"))));

        addPositive(Arrays.asList(
                Arrays.asList(new A("t"), new A("o")),
                Arrays.asList(new A("t"))));

        addPositive(Arrays.asList(
                Arrays.asList(new A("t"), new A("o")),
                Arrays.asList(new A("o"), new A("n"))));

        addPositive(Arrays.asList(
                Arrays.asList(new A("t"), new A("o")),
                Arrays.asList(new A("o"), new A("n")),
                Arrays.asList(new A("a"), new A("b"), new A("c"), new A("d"))));

        addPositive(Arrays.asList(
                Arrays.asList(new A("t"), new A("o"), new A("d"), new A("a"), new A("y")),
                Arrays.asList(new A("t"), new A("o")),
                Arrays.asList(new A("t"), new A("o"), new A("m"), new A("o"), new A("r"), new A("r"), new A("o"), new A("w")),
                Arrays.asList(new A("a"), new A("b"), new A("c"), new A("d"))));
    }

    @Test
    public void manyKeys() throws IOException {
        addPositive(A.neos(getManyKeys()));
    }
}
