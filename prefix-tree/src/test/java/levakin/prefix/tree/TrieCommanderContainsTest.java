package levakin.prefix.tree;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

public class TrieCommanderContainsTest extends Tester<A>
{
    public Trie<A> oneNodeTrie(A key) {
        return new HashTrie<A>(new HashMap<A, Trie<A>>()
        {{
            put(key, factory.initTrie());
        }});
    }

    @Test
    public void emptyTree(){
        Assert.assertFalse(commander.contains(Arrays.asList(new A("1"), new A("2")), factory.initTrie()));
        Assert.assertFalse(commander.contains(Collections.singletonList(new A("a")), oneNodeTrie(new A("x"))));
    }

    @Test
    public void oneNode(){
        A candidate = new A("t");
        A candidateClone = new A("t");
        Trie<A> root = factory.initTrie();
        root.getPrefixToTrie().put(candidate, factory.initStopTrie());
        Assert.assertTrue(commander.contains(Collections.singletonList(candidateClone), root));
    }

    @Test
    public void invisibleChar(){
        Trie<A> root = factory.initTrie();
        root.getPrefixToTrie().put(new A("\u0000"), factory.initStopTrie());
        Assert.assertTrue(commander.contains(Collections.singletonList(new A("\u0000")), root));
    }

    @Test
    public void oneNodeNegative(){
        Trie<A> root = oneNodeTrie(new A("y"));
        assertNotContains(root, Arrays.asList(new A(""), new A("a"), new A("zyx")));
    }

    public Trie<A> getTwoLettersTree() {
        Trie<A> trie = oneNodeTrie(new A("a"));
        Trie<A> child = trie.getPrefixToTrie().get(new A("a"));
        child.getPrefixToTrie().put(new A("t"), factory.initStopTrie());
        child.getPrefixToTrie().put(new A("m"), factory.initStopTrie());
        return trie;
    }
    @Test
    public void twoLettersTree(){
        assertContains(getTwoLettersTree(), A.neos(new String[]{"a", "t"}));
        assertContains(getTwoLettersTree(), A.neos(new String[]{"a", "m"}));
    }

    @Test
    public void twoLettersNegative(){
        assertNotContains(getTwoLettersTree(), A.neos(new String[]{"a"}));
        assertNotContains(getTwoLettersTree(), A.neos(new String[]{"a", "a"}));
        assertNotContains(getTwoLettersTree(), A.neos(new String[]{"t", "a"}));
        assertNotContains(getTwoLettersTree(), A.neos(new String[]{"m", "a"}));
        assertNotContains(getTwoLettersTree(), A.neos(new String[]{"a", "t", "m"}));
        assertNotContains(getTwoLettersTree(), A.neos(new String[]{"a", "m", "t"}));
    }

    //creates a tree of ["to" "today"]
    public Trie<A> getTwoWordsTree(){
        Trie<A> root = factory.initTrie();
        root.getPrefixToTrie().put(new A("t"), factory.initTrie());

        Trie<A> child = root.getPrefixToTrie().get(new A("t"));
        child.getPrefixToTrie().put(new A("o"), factory.initTrie());

        child = child.getPrefixToTrie().get(new A("o"));
        child.setIsStop(true);
        child.getPrefixToTrie().put(new A("d"), factory.initTrie());

        child = child.getPrefixToTrie().get(new A("d"));
        child.getPrefixToTrie().put(new A("a"), factory.initTrie());

        child = child.getPrefixToTrie().get(new A("a"));
        child.getPrefixToTrie().put(new A("y"), factory.initStopTrie());
        return root;
    }
    @Test
    public void twoWords1(){
        assertContains(getTwoWordsTree(), A.neos(new String[]{"t", "o"}));
    }

    @Test
    public void twoWords2(){
        assertContains(getTwoWordsTree(), A.neos(new String[]{"t", "o", "d", "a", "y"}));
    }

    @Test
    public void twoWordsNegative(){
        assertNotContains(getTwoWordsTree(), A.neos(new String[]{"t", "o", "d"}));
        assertNotContains(getTwoWordsTree(), A.neos(new String[]{"t", "o", "m", "o", "r", "r", "o", "w"}));
    }
}