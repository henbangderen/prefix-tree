package levakin.prefix.tree;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class Tester<E> {
    private final String testDir = "./src/test/resource";
    Logger log = LoggerFactory.getLogger(getClass());
    TrieCommander<E> commander = new TrieCommander<>();
    TrieFactory<E> factory = new TrieFactory<>();

    public void assertContains(Trie<E> tree, List<E> candidate) {
        Assert.assertTrue("Candidate must be found: '" + candidate + "' in '" + tree + "'",
                commander.contains(candidate, tree));
    }

    public void assertNotContains(Trie<E> tree, List<E> candidate) {
        Assert.assertFalse("Candidate must not be found: '" + candidate + "' in '" + tree + "'",
                commander.contains(candidate, tree));
    }

    public List<String> getManyKeys() throws IOException {
        String raw = new String(Files.readAllBytes(Paths.get(testDir + "/lorem-ipsum.txt")));
        List<String> keys = Arrays.asList(raw.split("\\W+"));
        //log.info(keys.toString());
        return keys;
    }

    public void addPositive(List<List<E>> keys){
        Trie<E> left = factory.initTrie();
        commander.addAll(left, keys);

        Trie<E> right = factory.initTrie();
        Collections.reverse(keys);
        commander.addAll(right, keys);

        for(List<E> key : keys){
            assertContains(left, key);
        }
        for(List<E> key : keys){
            assertContains(right, key);
        }
        Assert.assertEquals(left.toString(), right.toString());
    }
    public void removePositive(List<List<E>> keys){
        Trie<E> left = factory.initTrie();
        commander.addAll(left, keys);

        Trie<E> right = factory.initTrie();
        Collections.reverse(keys);
        commander.addAll(right, keys);

        commander.removeAll(left, keys);
        log.info("left: " + left);
        commander.removeAll(right, keys);
        log.info("right: " + right);

        for(List<E> key : keys){
            assertNotContains(left, key);
        }
        for(List<E> key : keys){
            assertNotContains(right, key);
        }
        Assert.assertEquals(left.toString(), right.toString());
    }
}
