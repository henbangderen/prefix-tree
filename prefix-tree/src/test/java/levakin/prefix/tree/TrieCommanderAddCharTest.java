package levakin.prefix.tree;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TrieCommanderAddCharTest extends Tester<Character> {

    @Test
    public void manyKeys() throws IOException {
        List<List<Character>> keys = new ArrayList<>();
        for (String key : getManyKeys()){
            List<Character> charKey = new ArrayList<>();
            for (Character c : key.toCharArray()){
                charKey.add(c);
            }
            keys.add(charKey);
        }
        addPositive(keys);
    }
}
