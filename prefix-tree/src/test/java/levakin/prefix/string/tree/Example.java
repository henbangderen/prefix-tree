package levakin.prefix.string.tree;

import levakin.prefix.tree.Trie;
import levakin.prefix.tree.TrieFactory;
import org.junit.Test;

import java.util.Arrays;

public class Example {

    public void example(){
    CharacterTrieCommander commander = new CharacterTrieCommander();
    TrieFactory<Character> factory = new TrieFactory();
    //add
    Trie<Character> tree = commander.add(factory.initTrie(), Arrays.asList("today", "to", "tomorrow", "abc"));

    //contains
    System.out.println(commander.contains("to", tree));  //true
    System.out.println(commander.contains("tomorrow", tree)); //true
    System.out.println(commander.contains("t", tree)); //false
    System.out.println(commander.contains("I do not exist", tree)); //false

    //remove
    commander.remove(tree, Arrays.asList("to", "abc"));
    System.out.println(commander.contains("to", tree));  //false
    System.out.println(commander.contains("abc", tree));  //false
    System.out.println(commander.contains("today", tree));  //true
    }

    @Test
    public void runExample(){
        example();
    }
}
