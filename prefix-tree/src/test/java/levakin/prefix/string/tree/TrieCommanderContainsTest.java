package levakin.prefix.string.tree;

import levakin.prefix.tree.*;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class TrieCommanderContainsTest extends Tester<Character>
{
    Logger log = LoggerFactory.getLogger(getClass());
    CharacterTrieCommander commander = new CharacterTrieCommander();
    TrieFactory<Character> factory = new TrieFactory<>();

    public Trie<Character> oneNodeTrie(Character c) {
        return new HashTrie<>(new HashMap<Character, Trie<Character>>()
        {{
            put(c, factory.initTrie());
        }});
    }

    @Test
    public void emptyTree(){
        Assert.assertFalse(commander.contains("I do not exist", factory.initTrie()));
        Assert.assertFalse(commander.contains("y", oneNodeTrie('x')));
    }

    @Test
    public void oneNode(){
        Trie<Character> root = factory.initTrie();
        root.getPrefixToTrie().put('t', factory.initStopTrie());
        Assert.assertTrue(commander.contains("t", root));
    }

    @Test
    public void invisibleChar(){
        Trie<Character> root = factory.initTrie();
        root.getPrefixToTrie().put('\0', factory.initStopTrie());
        Assert.assertTrue(commander.contains("\0", root));
    }

    @Test
    public void oneNodeNegative(){
        Trie<Character> root = oneNodeTrie('t');
        assertNotContains(root, commander.stringToChars(""));
        assertNotContains(root, commander.stringToChars("a"));
        assertNotContains(root, commander.stringToChars("zyx"));
    }

    public Trie<Character> getTwoLettersTree() {
        Trie<Character> trie = oneNodeTrie('a');
        Trie<Character> child = trie.getPrefixToTrie().get('a');
        child.getPrefixToTrie().put('t', factory.initStopTrie());
        child.getPrefixToTrie().put('m', factory.initStopTrie());
        return trie;
    }
    @Test
    public void twoLettersTree(){
        Trie<Character> twoLettersTree = getTwoLettersTree();
        assertContains(twoLettersTree, commander.stringToChars("at"));
        assertContains(twoLettersTree, commander.stringToChars("am"));
    }

    @Test
    public void twoLettersNegative(){
        Trie<Character> twoLettersTree = getTwoLettersTree();
        assertNotContains(twoLettersTree, commander.stringToChars("a"));
        assertNotContains(twoLettersTree, commander.stringToChars("aa"));
        assertNotContains(twoLettersTree, commander.stringToChars("ta"));
        assertNotContains(twoLettersTree, commander.stringToChars("ma"));
        assertNotContains(twoLettersTree, commander.stringToChars("atm"));
        assertNotContains(twoLettersTree, commander.stringToChars("amt"));
    }

    //creates a tree of ["to" "today"]
    public Trie<Character> getTwoWordsTree(){
        Trie<Character> root = factory.initTrie();
        root.getPrefixToTrie().put('t', factory.initTrie());

        Trie<Character> child = root.getPrefixToTrie().get('t');
        child.getPrefixToTrie().put('o', factory.initTrie());

        child = child.getPrefixToTrie().get('o');
        child.setIsStop(true);
        child.getPrefixToTrie().put('d', factory.initTrie());

        child = child.getPrefixToTrie().get('d');
        child.getPrefixToTrie().put('a', factory.initTrie());

        child = child.getPrefixToTrie().get('a');
        child.getPrefixToTrie().put('y', factory.initStopTrie());
        return root;
    }
    @Test
    public void twoWords(){
        Trie<Character> getTwoWordsTree = getTwoWordsTree();
        assertContains(getTwoWordsTree, commander.stringToChars("to"));
        assertContains(getTwoWordsTree, commander.stringToChars("today"));
    }

    @Test
    public void twoWordsNegative(){
        Trie<Character> getTwoWordsTree = getTwoWordsTree();
        assertNotContains(getTwoWordsTree, commander.stringToChars("tod"));
        assertNotContains(getTwoWordsTree, commander.stringToChars("tomorrow"));
    }
}