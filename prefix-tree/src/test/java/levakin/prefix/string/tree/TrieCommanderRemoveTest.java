package levakin.prefix.string.tree;

import levakin.prefix.tree.Tester;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;

public class TrieCommanderRemoveTest extends Tester<Character> {

    Logger log = LoggerFactory.getLogger(getClass());
    CharacterTrieCommander commander = new CharacterTrieCommander();

    @Test
    public void simplest(){
        removePositive(commander.stringsToCharsList(Arrays.asList("t", "o")));
    }

    @Test
    public void subKeyInKey(){
        removePositive(commander.stringsToCharsList(Arrays.asList("to", "t")));
    }

    @Test
    public void smoke() {
        removePositive(commander.stringsToCharsList(Arrays.asList("to", "on", "abcd")));
        removePositive(commander.stringsToCharsList(Arrays.asList("today", "to", "tomorrow", "abc")));
    }

    @Test
    public void manyKeys() throws IOException {
        removePositive(commander.stringsToCharsList(getManyKeys()));
    }
}
