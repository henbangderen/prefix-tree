package levakin.prefix.string.tree;

import levakin.prefix.tree.Tester;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class TrieCommanderAddTest extends Tester<Character> {

    Logger log = LoggerFactory.getLogger(getClass());
    CharacterTrieCommander commander = new CharacterTrieCommander();

    @Test
    public void smoke(){
        System.out.println(new File(".").getAbsolutePath());
        addPositive(commander.stringsToCharsList(Arrays.asList("t", "o")));
        addPositive(commander.stringsToCharsList(Arrays.asList("to", "on")));
        addPositive(commander.stringsToCharsList(Arrays.asList("to", "on", "abcd")));
        addPositive(commander.stringsToCharsList(Arrays.asList("today", "to", "tomorrow", "abc")));
    }

    @Test
    public void manyKeys() throws IOException {
        addPositive(commander.stringsToCharsList(getManyKeys()));
    }
}
