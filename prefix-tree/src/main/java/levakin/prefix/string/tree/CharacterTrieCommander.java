package levakin.prefix.string.tree;

import levakin.prefix.tree.Trie;
import levakin.prefix.tree.TrieCommander;

import java.util.ArrayList;
import java.util.List;

public class CharacterTrieCommander {
    public TrieCommander<Character> commander = new TrieCommander<>();

    public List<Character> stringToChars(String candidate){
        List<Character> result = new ArrayList<>();
        for (Character character : candidate.toCharArray()){
            result.add(character);
        }
        return result;
    }

    public List<List<Character>> stringsToCharsList(List<String> keys) {
        List<List<Character>> characterKeys = new ArrayList<>();
        for (String key : keys){
            characterKeys.add(stringToChars(key));
        }
        return characterKeys;
    }

    public boolean contains(String candidate, Trie<Character> trie) {
        return commander.contains(stringToChars(candidate), trie);
    }

    public Trie<Character> add(Trie<Character> trie, List<String> keys){
        //some validations may go here
        List<List<Character>> characterKeys = stringsToCharsList(keys);
        return commander.addAll(trie, characterKeys);
    }

    public Trie<Character> remove(Trie<Character> trie, List<String> keys){
        //some validations may go here
        List<List<Character>> characterKeys = stringsToCharsList(keys);
        return commander.removeAll(trie, characterKeys);
    }
}
