package levakin.prefix.tree;

import java.util.List;

class AddCommand<E> {
    TrieFactory<E> factory = new TrieFactory<>();

    public Trie<E> addAll(Trie<E> tree, List<List<E>> listOfkeys){
        for (List<E> keys : listOfkeys){
            add(tree, keys);
        }
        return tree;
    }

    public Trie<E> add(Trie<E> tree, List<E> keys) {
        Trie<E> subtree = tree;
        for (E key : keys) {
            if (!subtree.getPrefixToTrie().containsKey(key)) {
                subtree.getPrefixToTrie().put(key, factory.initTrie());
            }
            subtree = subtree.getPrefixToTrie().get(key);
        }
        subtree.setIsStop(true);
        return tree;
    }
}
