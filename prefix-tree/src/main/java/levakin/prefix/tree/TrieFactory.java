package levakin.prefix.tree;

import java.util.HashMap;

public class TrieFactory<E> {
    public HashTrie<E> initTrie() {
        return new HashTrie<>(new HashMap<>());
    }

    public Trie<E> initStopTrie() {
        Trie<E> trie = initTrie();
        trie.setIsStop(true);
        return trie;
    }
}