package levakin.prefix.tree;

import java.util.List;

class ContainsCommand<T> {

    //     t
    //    /
    //   o - this node should be returned
    //  /
    // (LIEF mark)

    //     t
    //    /
    //   o - this node should be returned if 'to' is searched
    //  / \
    // y   (LIEF mark)
    public Trie<T> getLastTrie(Trie<T> trie, List<T> prefixes) {
        Trie<T> child = trie;
        Trie<T> lastNonEmptyTrie = null;
        for (T prefix : prefixes){
            if (child.getPrefixToTrie().containsKey(prefix)) {
                lastNonEmptyTrie = child;
                child = child.getPrefixToTrie().get(prefix);
            }
            else {
                return null;
            }
        }
        if (child.isStop()){
            return lastNonEmptyTrie;
        }
        return null;
    }

    public boolean contains(List<T> prefixes, Trie<T> trie) {
        return null != getLastTrie(trie, prefixes);
    }
}
