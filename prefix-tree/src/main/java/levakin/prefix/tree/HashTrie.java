package levakin.prefix.tree;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class HashTrie<E> implements Trie<E> {
    private final Map<E, Trie<E>> prefixToTrie;
    private boolean isStop = false;

    public HashTrie(Map<E, Trie<E>> prefixToTrie) {
        this.prefixToTrie = prefixToTrie;
    }

    public boolean isStop(){
        return isStop;
    }

    @Override
    public void setIsStop(boolean isStop) {
        this.isStop = isStop;
    }

    public boolean isEmpty(){
        return prefixToTrie.isEmpty();
    }

    public Map<E, Trie<E>> getPrefixToTrie() {
        return prefixToTrie;
    }

    @Override
    public String toString() {
        if (prefixToTrie == null) return "<NULL>";
        TreeMap ordered = new TreeMap(Comparator.nullsFirst(Comparator.naturalOrder()));
        ordered.putAll(prefixToTrie);;
        return ordered.toString();
    }
}
