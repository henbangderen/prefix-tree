package levakin.prefix.tree;

import java.util.List;

public class RemoveCommand<E> {
    private final ContainsCommand<E> c = new ContainsCommand<>();

    public Trie<E> removeAll(Trie<E> tree, List<List<E>> keys){
        for (List<E> key : keys){
            remove(tree, key);
        }
        return tree;
    }

    public Trie<E> remove(Trie<E> tree, List<E> key) {
        if (!c.contains(key, tree)){
            return tree;
        }

        for (int i = key.size(); i > 0; i--) {
            List<E> subkey = key.subList(0, i);
            Trie<E> trieWhereRemove = c.getLastTrie(tree, subkey);
            if (trieWhereRemove == null){
                return tree;
            }
            E subkeyPrefix = key.get(subkey.size() - 1);
            Trie<E> maybeLief = trieWhereRemove.getPrefixToTrie().get(subkeyPrefix);
            //remove a leaf mark from ['toy' 'to'] -> ['toy']
            //     t
            //    /
            //   o
            //  / \
            // y   (o is LIEF)
            if (maybeLief.isStop() && !maybeLief.isEmpty()) {
                maybeLief.setIsStop(false);
                return tree;
            }

            //remove leaf prefixes from the bottom to up or return
            //        t      t     t    (t)
            //       /      /     /
            //      o      o    (o)
            //     /      /
            //    y     (y)
            //   /
            // (s)
            if (maybeLief.isStop()){
                trieWhereRemove.getPrefixToTrie().remove(subkeyPrefix);
            }
        }
        return tree;
    }

//    public Trie<E> remove(Trie<E> tree, List<E> key) {
//        if (!c.contains(key, tree)){
//            return tree;
//        }
//
//        for (int i = key.size(); i > 0; i--) {
//            List<E> subkey = key.subList(0, i);
//            Trie<E> trieWhereRemove = c.getLastTrie(tree, subkey);
//            if (trieWhereRemove == null){
//                return tree;
//            }
//            E subkeyPrefix = key.get(subkey.size() - 1);
//            Trie<E> maybeLief = trieWhereRemove.getPrefixToTrie().get(subkeyPrefix);
//            //remove a leaf mark from ['toy' 'to'] -> ['toy']
//            //     t
//            //    /
//            //   o
//            //  / \
//            // y   (o is LIEF)
//            if (maybeLief.isStop() && !maybeLief.isEmpty()) {
//                maybeLief.setIsStop(true);
//                return tree;
//            }
//
//            //remove leaf prefixes from the bottom to up or return
//            //        t      t     t    (t)
//            //       /      /     /
//            //      o      o    (o)
//            //     /      /
//            //    y     (y)
//            //   /
//            // (s)
//            if (maybeLief.isStop()){
//                trieWhereRemove.getPrefixToTrie().remove(subkeyPrefix);
//            }
//        }
//        return tree;
//    }
}
