package levakin.prefix.tree;

import java.util.Map;

public interface Trie<T> {
    boolean isStop();
    void setIsStop(boolean isLeaf);
    boolean isEmpty();
    Map<T, Trie<T>> getPrefixToTrie();
}
