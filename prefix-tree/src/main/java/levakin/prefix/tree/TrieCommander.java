package levakin.prefix.tree;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class TrieCommander<E> {
    Logger log = LoggerFactory.getLogger(getClass());

    public boolean contains(List<E> candidates, Trie<E> tree) {
        if (candidates.isEmpty()){
            log.warn("Candidate string is empty");
            return false;
        }
        if (tree.isEmpty()){
            log.warn("Tree is empty");
            return false;
        }
        return new ContainsCommand<E>().contains(candidates, tree);
    }

    public Trie<E> addAll(Trie<E> tree, List<List<E>> keys){
        //some validations may go here
        return new AddCommand<E>().addAll(tree, keys);
    }
    public Trie<E> add(Trie<E> tree, List<E> keys){
        //some validations may go here
        return new AddCommand<E>().add(tree, keys);
    }

    public Trie<E> removeAll(Trie<E> tree, List<List<E>> keys){
        //some validations may go here
        return new RemoveCommand<E>().removeAll(tree, keys);
    }
    public Trie<E> remove(Trie<E> tree, List<E> keys){
        //some validations may go here
        return new RemoveCommand<E>().remove(tree, keys);
    }

}
